package com.colisweb.distances.providers.here

import cats.effect.IO
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers
import requests.Response
import cats.effect.unsafe.implicits.global

class RequestExecutorTest extends AnyFlatSpec with Matchers {

  "SyncRequestExecutor with Cats" should behave like correctlyCatchNonFatalErrors[IO](
    new SyncRequestExecutor[IO](),
    _.attempt.unsafeRunSync()
  )

  "AsyncRequestExecutor with Cats" should behave like correctlyCatchNonFatalErrors[IO](
    new AsyncRequestExecutor[IO](),
    _.attempt.unsafeRunSync()
  )

  private def correctlyCatchNonFatalErrors[F[_]](
      executor: RequestExecutor[F],
      runEffect: F[Response] => Either[Throwable, Response]
  ) = {
    val error                     = new Exception("Boom")
    def failingResponse: Response = { throw error }
    val result                    = runEffect(executor.run(failingResponse))
    result shouldBe Left(error)
    ()
  }
}
